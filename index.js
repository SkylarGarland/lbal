try
{
	var express = require('express');
	var app = express();
	var port = process.env.PORT || 8080;

	app.use(express.json());

	let MongoClient = require('mongodb').MongoClient;
	let uri = "mongodb+srv://root:RCeL9ayiezBLaCz@cluster0.gvbea.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

	function UseDB(onUse)
	{
		let client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
		return new Promise((onSuccess, onFail) =>
		{
			client.connect(async (err) =>
			{
				if (err)
				{
					onFail(err);
				}
				else
				{
					const collection = client.db("test").collection("items");

					let result = await onUse(collection);

					onSuccess(result);
					client.close();
				}
			});
		});
	}

	let Rarity = {
		"common":"common",
		"uncommon":"uncommon",
		"rare":"rare"
	}

	let items = [
		GameItem("Magpie", Rarity.common, "Gives 9 gold every 4 spins.", -1),
		GameItem("King Midas", Rarity.rare, "Adds 1 gold each turn. Adjacent gold gives 3x more gold.", 2),
		GameItem("Goose", Rarity.common, "Has a 1% chance of adding a Golden Egg.", 1),
		GameItem("Bee", Rarity.uncommon, "Adjacent flowers give 2x more gold.", 1),
		GameItem("Golden Egg", Rarity.rare, "", 3),
		GameItem("Cat", Rarity.common, "", 1),
		GameItem("Void Stone", Rarity.uncommon, "Adjacent empty squares give 1 gold more. Destroys itself if adjacent to 0 empty squares. Gives 8 gold when destroyed.", 0),
	]

	function GameItem(name, rarity, description, gpt)
	{
		if (rarity === undefined && description === undefined && gpt === undefined)
		{
			return {
				"name": name.name,
				"rarity": name.rarity,
				"description": name.description,
				"goldPerTurn": name.goldPerTurn
			};
		}
		else
		{
			return {
				"name": name,
				"rarity": rarity,
				"description": description,
				"goldPerTurn": gpt
			};
		}
	}

	async function Init()
	{
		await UseDB(async (dbItems) =>
		{
			let count = await dbItems.countDocuments();
			if (count === 0)
			{
				console.log("Initializing database...");
				await dbItems.insertMany(items);
			}
			else
			{
				console.log("Database already initialized.");
			}
		});
	}

	async function GetAllItems()
	{
		return await UseDB(async (dbItems) =>
		{
			let contents = await dbItems.find().toArray();
			var result = [];

			for (var i = 0; i < contents.length; i++)
			{
				result[i] = GameItem(contents[i]);
			}

			return result;
		});
	}

	async function GetItem(name)
	{
		return await UseDB(async (dbItems) =>
		{
			let iterator = dbItems.find({ name: name });

			if (await iterator.hasNext())
			{
				return GameItem(await iterator.next());
			}
			else
			{
				return false;
			}
		});
	}

	async function CreateItem(name, rarity, description, gpt)
	{
		await UseDB(async (dbItems) =>
		{
			await dbItems.insertOne(GameItem(name, rarity, description, gpt));
		});
	}

	async function DeleteItem(name)
	{
		return await UseDB(async (dbItems) =>
		{
			let result = await dbItems.deleteOne({ name: name });
			return result.deletedCount > 0;
		});
	}

	app.get("/api/items", async (req, res) => {
		try
		{
			res.status(200).send(await GetAllItems());
		}
		catch (e)
		{
			console.log(e);
			res.status(500).send({"error":"An internal server error occured."});
		}
	})

	app.get("/api/items/:item_name", async (req, res) => {
		try
		{
			let name = req.params.item_name;
			let result = await GetItem(name);

			if (result)
			{
				res.status(200).send(result);
			}
			else
			{
				res.status(404).send({"error":`No item named ${name}.`});
			}
		}
		catch (e)
		{
			console.log(e);
			res.status(500).send({"error":"An internal server error occured."});
		}
	})

	app.post("/api/items", async (req, res) => {
		try
		{
			let newItem = req.body;
			
			await CreateItem(newItem.name, newItem.rarity, newItem.description, newItem.goldPerTurn);

			res.status(201).send();
		}
		catch (e)
		{
			console.log(e);
			res.status(500).send({"error":"An internal server error occured."});
		}
	})

	app.put("/api/items/:item_name", (req,res) => {
		try
		{
			res.status(501).send({"error":"This endpoint has not been implemented and will be implemented in the future."});
		}
		catch (e)
		{
			console.log(e);
			res.status(500).send({"error":"An internal server error occured."});
		}
	})

	app.delete("/api/items/:item_name", async (req,res) => {
		try
		{
			let name = req.params.item_name;
			let result = await DeleteItem(name);

			if (result)
			{
				res.status(200).send();
			}
			else
			{
				res.status(404).send({"error":`No item named ${name}.`});
			}
		}
		catch (e)
		{
			console.log(e);
			res.status(500).send({"error":"An internal server error occured."});
		}
	})

	Init();

	const onHttpStart = () => console.log(`Server has started and is listening on port ${port}`);

	app.listen(port, onHttpStart);
}
catch (ex)
{
	console.log(ex);
	console.log(ex.stack);
}